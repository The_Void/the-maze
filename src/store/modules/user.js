export const user = {
    namepaced: true,
    state: {
        name: '',
        email: '',
        password: ''
    },
    mutation: {
        SET_NAME: (state, value) => {
            state.name = value;
        },
        SET_EMAIL: (state, value) => {
            state.email = value;
        },
        SET_PASSWORD: (state, value) => {
            state.password = value;
        }
    }
}