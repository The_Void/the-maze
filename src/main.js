import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import VeeValidate from 'vee-validate';
import _ from 'lodash';

Vue.use(VeeValidate);
Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App),
	mounted() {
		this.$router.replace('/');
	}
}).$mount('#app')
