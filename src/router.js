import Vue from 'vue'
import Router from 'vue-router'
import Door_1 from './views/Door_1.vue'

Vue.use(Router)

export default new Router({
	mode: 'abstract',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'door_1',
			component: Door_1
		},
		{
			path: '/door_2',
			name: 'door_2',
			component: () => import('./views/Door_2.vue')
		}
	]
})
